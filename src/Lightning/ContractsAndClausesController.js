({
    //initialize function to retrieve the current contracts from the database and render them 
    //in a dropdown list
	init : function(component, event, helper) {
        let action = component.get("c.fetchContracts");
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let contractList = response.getReturnValue();
                contractList.unshift("Select a Contract");
                component.set("v.contracts", contractList);
            }
        })
        $A.enqueueAction(action);  
	},
    
    //function to retrieve and display details about he selected contract record by sending
    //the contract name to the apex controller
    getContractDetails : function(component, event, helper) {       
        let contractInput = component.find("contractSelect").get("v.value");
        if (contractInput === "Select a Contract") { return; }
        helper.callContractApex(component, contractInput);     
    },
    
    //function to handle clicks from the new button
    handleNew : function(component, event, helper) {
       
        //determining what clauses are on this record based on the v.data attribute
        let currentClauses = [];
        let currentData = component.get("v.data");
        
        for (let i=0; i<currentData.length; i++) {
            currentClauses.push(currentData[i].Name);
        }

        //calling apex method and passing list of current clauses so that it can return list of
        //unused ones
        helper.callUnusedClausesApex(component, currentClauses);
    },

	//whenever save button is clicked on modal    
    handleSave : function(component, event, helper) {
        //this value is set by the loadBools function below
        let selectedClause = component.get("v.selectedClause");
        component.set("v.modalToggle", false);
        //if nothing selected, then stop execution
        if (selectedClause.length == 0) { return; }
        //calling helper that calls apex to save selected clauses
        helper.commitClauses(component, selectedClause);  
    },
    
    //simple method to hide modal and clear the selectedClause attribute if any clauses were selected
    handleCancel : function(component, event, helper) {  
		component.set("v.modalToggle", false);
		component.set("v.selectedClause", []);            
    },
    
    loadBools : function(component, event, helper) {
        //a simple algorithm triggered once every time a checkbox associated with an unused contract
        //clause is clicked. Ultimate goal is to load an array, v.selectedClause, with names of all
        //clauses to be saved. If the user clicks a checkbox for the first time, then that clause is
        //loaded into the array. If the same checkbox is clicked again, it will remove it and exit execution
        //that way, user can select and unselect in any order and the selected clauses will be 
        //available for other functions for database manipulation
        let target = event.getSource();
        let targetLabel = target.get("v.label");
        let selectedClause = component.get("v.selectedClause");
        
        for (let i=0; i<selectedClause.length; i++) {
            if (targetLabel == selectedClause[i]){
                selectedClause.splice(i, 1);
                return;
            }
        }
 
        selectedClause.push(targetLabel);  
    },
    
    //handling click on the on-record delete action
    handleDelete : function(component, event, helper) {
        //determining the reference to the clicked row by using events and passing that into 
        //a helper that calls apex to delete that record
        let row = event.getParam("row");
        helper.removeRecord(component, row);
    },
    
    //---- toggles for loading spinner during apex transactions ----//
    showSpinner : function(component) {
       component.set("v.spinnerToggle", true);
    },
    
    hideSpinner : function(component) {
       component.set("v.spinnerToggle", false); 
    }
})
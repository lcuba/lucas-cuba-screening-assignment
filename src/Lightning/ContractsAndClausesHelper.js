({
    callContractApex : function(component, contName) {
        let action = component.get("c.fetchContract");
        action.setParams({contname: contName});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let contractDetails = response.getReturnValue();
                component.set("v.contractDetails", contractDetails);
                component.set("v.bodyToggle", true);
                //populating datatable
                this.tablePopulate(component);
            }
        })
        $A.enqueueAction(action);    
    },
    
    callUnusedClausesApex : function(component, currentClauses) {
    	let action = component.get("c.fetchUnusedClause");
        action.setParams({usedClause: currentClauses});
        action.setCallback(this, function(response) {
			let state = response.getState();
            if (state === "SUCCESS") {
                let clauseChoices = response.getReturnValue();
                if (clauseChoices.length == 0) {
                    //alerting user that there are no ununsed contract clauses and exiting execution
                    alert("No more unique contract clauses can be added");
                    return; 
                }
                component.set("v.potentialClauses", clauseChoices);
                component.set("v.modalToggle", true);
            }
        })
        $A.enqueueAction(action);  
    },
    
    //populating datatable with related clauses
	tablePopulate : function(component) {
		let action = component.get("c.fetchAssignment");
        action.setParams({contname: component.find("contractSelect").get("v.value")});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let clauseList = response.getReturnValue();
                
                let cols = [
                    {label: 'Name', fieldName: 'Name', type: 'text'},
                    {label: 'Type', fieldName: 'Type', type: 'text'},
                    {label: 'Description', fieldName: 'Description', type: 'text'},
                    {type: 'action', typeAttributes: {rowActions: [{label: 'Delete', name: 'delete'}]}}
                ];
                
                let data = [];
 
                for (let i=0; i<clauseList.length;i++) {
                    data.push({
                        Name: clauseList[i].Clause__r.Name, 
                    	Type: clauseList[i].Clause__r.Type__c,
                        Description: clauseList[i].Clause__r.Description__c 
                    });
                }
                  
                component.set("v.columns", cols);
                component.set("v.data", data);
            }
        })
        $A.enqueueAction(action);
	},
    
    commitClauses : function(component, clauses) {     
        let action = component.get("c.commitNewClause");
        action.setParams({commitClauses: clauses, contName: component.find("contractSelect").get("v.value")});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                //refreshing table
                this.tablePopulate(component);
            }
        })
        $A.enqueueAction(action);
    },
    
    removeRecord : function(component, row) {
        //getting name of clause associated with the contract clause assignment record to be deleted
        let rows = component.get('v.data');
        let rowIndex = rows.indexOf(row);
        let deletionName = rows[rowIndex].Name;
        
        let action = component.get("c.deleteClause");
        action.setParams({clauseName: deletionName, contName: component.find("contractSelect").get("v.value")});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                //refreshing table
                rows.splice(rowIndex, 1);
        		component.set('v.data', rows);
            }
        })
		$A.enqueueAction(action);    
    }
})